// Projeto Curto Circuito - Módulo HC12 – Comunicação Termite 

#include <SoftwareSerial.h> // Inclui Biblioteca
SoftwareSerial mySerial(2, 3);       // Simula RX e TX em outras portas
char input;                          // Armazena os caracteres recebidos
const int led1 = 13;                 // Led 1 porta digital 13
const int led2 = 12;                 // Led 2 porta digital 12
int state = 0;                       // Alterar o estado lógico do LED1 (0 e 1)
int state2 = 0;                      // Alterar o estado lógico do LED2 (0 e 1)
void setup() {
  Serial.begin(9600);                // Taxa de transferência do Computador
  mySerial.begin(9600);              // Taxa de transferência do HC12
  pinMode(led1, OUTPUT);             // Declara led 1 como saída
  pinMode(led2, OUTPUT);             // Declara led 2 como saída
}
void loop() {
  if (mySerial.available() > 0) {   // Se a conexão estiver acessível
    input = mySerial.read();        // input será igual ao valor recebido
    if (input == 'a')   {           // Se input for igual a 1
      state = !state;               // Inverte o valor de state
      digitalWrite(led1, state);    // Irá acender ou apagar de acordo com state
      if (state == 0) {             // Se state for igual a  0
        mySerial.println("Led 1 Desligado"); // Escreve a mensagem 
     }
      else {                        // Se state for diferente de  0
        mySerial.println("Led 1 Ligado");    // Escreve a mensagem 
      }
    }
    if (input == 'b')  {                  // Se input for igual a 2
      state2 = !state2;                   // Inverte o valor de state
      digitalWrite(led2, state2);         // Irá acender ou apagar de acordo state
      if (state2 == 0) {                  // Se state for igual a  0
        mySerial.println("Led 2 Desligado");
      }
      else {                              // Se state for diferente de  0
        mySerial.println("Led 2 Ligado");
      }
    }
  }
}
